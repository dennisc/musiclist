# musiclist

A crappy command-line interface for managing your musiclist

(You probably don't want to use this unless you really know what you are getting yourself into.)

## (Anti)-Features

- No tags, directory management, etc, if you really care then follow a naming convention for your songs

## Dependencies

### Runtime

Some subcommands have runtime dependencies.
- yt-dlp (sync)
- mpg123 (play)

## Configuration

- Set `$MUSICLIST_DOWNLOADS` to change the directory where songs are downloaded to
- Set `$MUSICLIST_SONGS` to change the file where the song list is stored
