use crate::paths::{download_dir, song_path};
use clap::{Parser, Subcommand};
use std::{fs, fs::OpenOptions, io::Write};

#[derive(Parser)]
#[clap(version, disable_help_subcommand = true)]
struct Cli {
    #[clap(subcommand)]
    subcommand: Command,
}

#[derive(Debug, Subcommand)]
enum Command {
    Add { name: String, url: String, comment: Option<String> },
    Sync,
    Rename { old: String, new: String },
    Remove { name: String },
    Play { names: Vec<String> },
    Show,
}

pub fn parse_args() {
    let args = Cli::parse();
    let mut song_file = OpenOptions::new()
        .create(true)
        .append(true)
        .open(song_path())
        .unwrap();
    match args.subcommand {
        Command::Add { name, url, comment } => {
            match fs::read_to_string(song_path()) {
                Ok(string) => {
                    for line in string.lines() {
                        let mut iter = line.split_whitespace();
                        let line_name = iter.next().unwrap();
                        if name == line_name {
                            panic!("Song with name \"{name}\" already exists");
                        }
                    }
                }
                Err(_) => panic!("Could not read {:?}", song_path()),
            }
            let contents = match comment {
                Some(comment) => format!("{name} {url} {comment}"),
                None => format!("{name} {url}"),
            };
            writeln!(song_file, "{contents}").unwrap();
        }
        Command::Rename { old, new } => match fs::read_to_string(song_path()) {
            Ok(string) => {
                song_file.set_len(0).unwrap();
                for line in string.lines() {
                    let mut iter = line.split_whitespace();
                    let line_name = iter.next().unwrap();
                    let line = &if old == line_name {
                        line.replacen(&old, &new, 1)
                    } else {
                        String::from(line)
                    };
                    writeln!(song_file, "{line}").unwrap();
                }
                fs::rename(download_dir().join(old), download_dir().join(new)).expect("Could not rename song");
            }
            Err(_) => panic!("Could not read {:?}", song_path()),
        },
        Command::Show => {
            let string = fs::read_to_string(song_path()).unwrap();
            match std::env::var("PAGER") {
                Ok(pager) => {
                    let mut process = match std::process::Command::new(&pager)
                        .arg(song_path().to_str().unwrap())
                        .spawn()
                    {
                        Err(msg) => panic!("Couldn't spawn {pager}: {msg}"),
                        Ok(process) => process,
                    };

                    process.wait().expect("`wait` failed");
                }
                Err(_) => {
                    println!("{}", string);
                }
            }
        }
        Command::Sync => match fs::read_to_string(song_path()) {
            Ok(string) => {
                for line in string.lines() {
                    let mut iter = line.split_whitespace();
                    let name = iter.next().unwrap();
                    let url = iter.next().unwrap();
                    let download_path = download_dir().join(String::from(name) + ".mp3");
                    if !download_path.exists() {
                        std::process::Command::new("yt-dlp")
                            .args([
                                "--extract-audio",
                                "--audio-format",
                                "mp3",
                                "--output",
                                &(String::from(download_dir().join(name).to_str().unwrap()) + ".%(ext)s"),
                                url,
                            ])
                            .spawn()
                            .expect("yt-dlp is required to sync songs");
                    }
                }
            }
            Err(_) => panic!("Could not read {:?}", song_path()),
        },
        Command::Remove { name } => match fs::read_to_string(song_path()) {
            Ok(string) => {
                song_file.set_len(0).unwrap();
                for line in string.lines() {
                    let line_name = line.split_whitespace().next().unwrap();
                    if name != line_name {
                        writeln!(song_file, "{line}").unwrap();
                    }
                }
            }
            Err(_) => panic!("Could not read {:?}", song_path()),
        },
        Command::Play { names } => {
            let mut process = std::process::Command::new("mpg123").args(names.iter().map(|name| String::from(download_dir().join(String::from(name) + ".mp3").to_str().unwrap()))).spawn().expect("mpg123 is required to play songs");
            process.wait().expect("`wait` failed");
        }
    }
}
