use dirs::{config_dir, data_dir};
use std::{env, path::PathBuf};

// Directory for *downloaded* songs
pub fn download_dir() -> PathBuf {
    match env::var("MUSICLIST_DOWNLOADS") {
        Ok(var) => PathBuf::from(var),
        Err(_) => data_dir().unwrap().join("musiclist"),
    }
}

// Path where songs are stored
pub fn song_path() -> PathBuf {
    match env::var("MUSICLIST_SONGS") {
        Ok(var) => PathBuf::from(var),
        Err(_) => config_dir().unwrap().join("musiclist"),
    }
}
